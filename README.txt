INTRODUCTION
------------
This module allows for a form to be rendered as a follow-up to another form
upon its submission. This is a developer module; you should not need to install
this module unless it is required by another module.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/marleythedog/2367247
 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2367247


REQUIREMENTS
------------
No special requirements


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/documentation/install/modules-themes/modules-7 for
   further information.


CONFIGURATION
-------------
The module has no UI or user modifiable settings.


USAGE (FOR DEVELOPERS)
----------------------
This module provides the form element type 'followup_form'.

An example of the simplest usage, which would show the search block form as
a follow-up to your form:
  $form['followup_form'] = array(
    '#type' => 'followup_form',
    '#form_id' => 'search_block_form',
  );

NOTICE: This module uses the #prefix and #suffix attributes on the form
element, and will override anything set on them in the form builder function.

The module supports using Webforms as follow-up forms as well, but requires
a different attribute, as calling the form builder directly on Webform forms is
fickle.  This would embed node 171, assuming it contains a Webform:
  $form['followup_form'] = array(
    '#type' => 'followup_form',
    '#webform_nid' => 171,
  );

To use this module on an AJAX-enabled form, you will need to create your AJAX
response wrapper in the #wrapper attribute of the followup_form element, then
define your AJAX submit callback as normal:
  $form['followup_form'] = array(
    '#type' => 'followup_form',
    '#form_id' => 'search_block_form',
    '#wrapper' => '<div id="calculation-result">%form</div>',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Calculate'),
    '#ajax' => array(
      'wrapper' => 'calculation-result',
      'callback' => 'followup_form_ajax_callback',
      'method' => 'replace',
    ),
  );
The function followup_form_ajax_callback() is an optional simple callback that
returns the $form['#suffix'].  If you need to do your own processing in the
AJAX callback, just make sure to include the $form['#suffix'] in your return
value.


MAINTAINERS
-----------
 * Chase Cathcart (marleythedog) - https://www.drupal.org/u/marleythedog


FAQ
---
No FAQ at this time
