<?php
/**
 * @file
 * Provides a form element type for defining a form to be rendered as a follow-
 * up to the current form upon submission.
 */

/**
 * Implements hook_element_info().
 */
function followup_form_element_info() {
  $types['followup_form'] = array(
    '#process' => array('followup_form_element_followup_form_process'),
    '#after_build' => array('followup_form_element_followup_form_after_build'),
    '#position' => 'auto',
    '#form_id' => NULL,
    '#form_arguments' => array(),
    '#webform_nid' => NULL,
    '#wrapper' => '%form',
  );

  return $types;
}

/**
 * Form process callback for followup_form element.
 */
function followup_form_element_followup_form_process($element, &$form_state, &$form) {
  $is_ajax = isset($form_state['input']['ajax_page_state']);
  $element['#form_id'] = $element['#webform_nid'] ? 'webform_client_form_' . $element['#webform_nid'] : $element['#form_id'];
  $element['content'] = array();

  if ($form_state['submitted'] || isset($_POST['form_id']) && $_POST['form_id'] == $element['#form_id']) {
    if ($element['#position'] == 'auto' && !$is_ajax) {
      $element['#position'] = 'before';
    }
    if ($element['#webform_nid']) {
      if (($node = node_load($element['#webform_nid'])) && node_access('view', $node)) {
        $element['content'] = node_view($node, 'embed');
        $element['#form'] =& $element['content']['webform']['#form'];
      }
    }
    elseif ($element['#form_id']) {
      $args = array($element['#form_id']);
      $args += $element['#form_arguments'];
      $element['content'] = call_user_func_array('drupal_get_form', $args);
      $element['#form'] =& $element['content'];
    }

    $element['#form']['#action'] = $form['#action'];
  }

  return $element;
}

/**
 * Form after_build callback for followup_form element.
 */
function followup_form_element_followup_form_after_build($element, &$form_state) {
  $form =& $form_state['complete form'];
  $position = $element['#position'] == 'before' ? '#prefix' : '#suffix';

  $form[$position] = str_replace('%form', drupal_render($element['content']), $element['#wrapper']);
}

/**
 * Basic AJAX submit callback for use with forms with followup_form elements.
 */
function followup_form_ajax_callback($form, &$form_state) {
  return $form['#suffix'];
}

/**
 * Implements hook_honeypot_form_protections_alter().
 */
function followup_form_honeypot_form_protections_alter(&$options, &$form) {
  // The appropriate CSS is not being attached to hide the honeypot field on an
  // AJAX request.  It might seem heavy-handed to completely remove the field
  // over that but many spambots aren't JS-aware anyway.
  if ($form['#action'] == '/system/ajax') {
    $options = array_diff($options, array('honeypot'));
  }
}
